type Outside = {
  can_download: string;
  count: string;
  PAGE_COUNT: string;
};

type Inside = {
  canDownload: boolean;
  count: number;
  pageCount: number;
};

export const mapper = new Map<string, any>([
  ["stringToBoolean", (value: string): boolean => value === "true"],
  ["stringToNumber", (value: string): number => +value],
]);

const IOMap = {
  can_download: {
    name: "canDownload",
    mapper: "stringToBoolean",
  },
  count: {
    name: "count",
    mapper: "stringToNumber",
  },
  PAGE_COUNT: {
    name: "pageCount",
    mapper: (value: string) => {
      return +value.split("_")[0];
    },
  },
};

export function universalMapper<O, I>(outside: O): I {
  const inside: I = Object.keys(outside).reduce(
    (acc, item) => ({
      ...acc,
      [IOMap[item].name]:
        typeof IOMap[item].mapper === "string"
          ? mapper.get(IOMap[item].mapper)(outside[item])
          : IOMap[item].mapper(outside[item]),
    }),
    {} as unknown as I
  );
  return inside;
}

const outsideA: Outside = {
  can_download: "true",
  count: "12",
  PAGE_COUNT: "5_PAGE",
};

console.log(outsideA, " swithTo ", universalMapper<Outside, Inside>(outsideA));
