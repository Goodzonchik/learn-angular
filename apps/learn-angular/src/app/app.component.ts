import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

import {universalMapper} from '@learn-angular/transformer'

@Component({
  standalone: true,
  imports: [ RouterModule],
  selector: 'learn-angular-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
  title = 'learn-angular';

  x = universalMapper;
}
